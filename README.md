# welcomeToKassandra

## THEME: Azure for Industries

### SUB-THEME: Retail

### Problem Statement

Predictive AI, machine learning, and analytics can help retailers unlock new opportunities with data. They're also reimagining their business using Mixed Reality solutions, IoT, hybrid cloud, and computer vision. Propose ideas to help retailers keep their supply chains agile with Microsoft Azure.

Tell us how your idea can help retailers to create personalized customer experiences across channels, empowering employees, and keeping their supply chains agile, and provide gain visibility across your end-to-end supply chain, and unlock innovative business models with secure and scalable cloud-based technology.

### Objective

Our proposal aims to achieve the following three objective with the help of our application "Kassandra"-

* Stock Management

* Business Management

* Employee Management

### Introduction 

Kassandra is a suite of software tools designed to give resellers with assistance and guidance to start up and build their companies and organisations in a more effective way. It strives to provide the precise and lasting unification of all sectors of traders and manufacturers. By curating analyzers, predictors, manager tools Kassandra aims to bridge the mammoth deficit in technology infrastructure present today in the organized/unorganized retail sector. Kassandra aims to make the supply chain as agile as it has ever been, right from the warehouse to the customer's doorstep using a wide array of machine learning and artificial intelligence services to build tools and add-ons on the web-platform. This enables the merchants to smartly manage their stock units, prepare for any demand spikes using demand forecasting feature on the platform avoiding any stock cold storage or wastage and capital loss.

### Kassandra Tour

### Web App for Merchants[Manufacturers/Retailers]

##### Landing Page

![Landing page](https://user-images.githubusercontent.com/43987867/124388204-7c976580-dcff-11eb-946e-f6563028cd04.png?v=4&s=200)

----

##### Secured Portal Signup and Login for Merchants

![Signup](https://user-images.githubusercontent.com/43987867/124388381-52927300-dd00-11eb-8562-9b786ffbf8ab.png)

![Login](https://user-images.githubusercontent.com/43987867/124388344-22e36b00-dd00-11eb-896a-96fbc21ccfef.png)

----

##### Portal Garage
     
![Portal backroom](https://user-images.githubusercontent.com/43987867/124388488-cd5b8e00-dd00-11eb-9909-c8599b164a8f.gif)

----

##### Business Analytics Dashboard

![Business Analytics](https://user-images.githubusercontent.com/43987867/124389056-20364500-dd03-11eb-973f-b89cc0506770.png)

![Business Analytics 2](https://user-images.githubusercontent.com/43987867/124389521-275e5280-dd05-11eb-842b-3f1e261da8d4.png)

----

##### Regional Sales Insights

![Regional Analytics](https://user-images.githubusercontent.com/43987867/124389160-9175f800-dd03-11eb-8319-4e4857ce15aa.png)

----

##### Smart Stock Manager

![Smart Stock Manager](https://user-images.githubusercontent.com/43987867/124389334-3abcee00-dd04-11eb-8e4c-b06b922b50a4.png)

----

##### Product Recommender

![Product Recommender](https://user-images.githubusercontent.com/43987867/124389446-bc148080-dd04-11eb-8b66-ebb1b55a69e6.png)

----

##### Price Trend Smart Recommender

![Price Trend Smart Recommender](https://user-images.githubusercontent.com/43987867/124389413-91c2c300-dd04-11eb-899d-224bda148683.png)

----

##### Add a Product and Allocate Stocks[Manufacturer]

![Add product](https://user-images.githubusercontent.com/43987867/124389716-064a3180-dd06-11eb-983b-fef30a25bc38.png)

----

##### Product Sales Forecast and Insights

![Product Sales Forecast and Insights](https://user-images.githubusercontent.com/43987867/124389218-d306a300-dd03-11eb-8455-9653f2dd1dae.png)


### Stack

* React
* React Native
* Nodejs
* Express
* Python
* MySQL
* Redis
* Babylon.js

****Azure Services:****

* Azure App Service
* Azure Static Web App
* Azure Kubernetes Service
* Azure Machine Learning Service
* Azure Database for MySQL Server
* Azure Storage
* Azure Cache for Redis
* Azure Blob Storage
* Azure Machine Learning Studio

### Future Patches

* To incorporate open source online business API's to thus stretching around the web for showcasing patterns and suggesting better methodologies to our registered merchants.
* To include certain digital market and commercial stages to work on the same side with their administrations and API's interfaces so we can assemble our project philosophy ideally

### Team

* Udit Singh- Data Science, Backend Developer, Front-End, Azure Cloud Development
* Saksham Tiwari- Website UI/UX Design, Front-End
* Arsh Pratap- App UI/UX Design and Frontend

### Thank You!



